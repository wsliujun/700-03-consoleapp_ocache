﻿using System.Collections.Generic;
using System.Linq;
using Autofac;
using NUnit.Framework;
using Orchard.Environment;

namespace ConsoleApp_OCache.Tests
{
    [TestFixture]
    public class OrchardStarterTests
    {
        [Test]
        public void DefaultOrchardHostInstanceReturnedByCreateHost()
        {
            var host = OrchardStarter.CreateHostContainer(b => b.RegisterInstance(new TestBuilder()));
            Assert.That(host, Is.TypeOf<IContainer>());
        }

        [Test]
        public void ContainerResolvesServicesInSameOrderTheyAreRegistered()
        {
            var container = OrchardStarter.CreateHostContainer(builder =>
            {
                builder.RegisterType<Component1>().As<IServiceA>();
                builder.RegisterType<Component2>().As<IServiceA>();
            });
            var services = container.Resolve<IEnumerable<IServiceA>>();
            Assert.That(services.Count(), Is.EqualTo(2));
            Assert.That(services.First(), Is.TypeOf<Component2>());
            Assert.That(services.Last(), Is.TypeOf<Component1>());
        }

        [Test]
        public void MostRecentlyRegisteredServiceReturnsFromSingularResolve()
        {
            var container = OrchardStarter.CreateHostContainer(builder =>
            {
                builder.RegisterType<Component1>().As<IServiceA>();
                builder.RegisterType<Component2>().As<IServiceA>();
            });
            var service = container.Resolve<IServiceA>();
            Assert.That(service, Is.Not.Null);
            Assert.That(service, Is.TypeOf<Component2>());
        }

        public interface IServiceA { }

        public class Component1 : IServiceA { }

        public class Component2 : IServiceA { }

        public class TestBuilder { }
    }
}