﻿using System;
using Autofac;
using NUnit.Framework;
using Orchard.Caching;

namespace Orchard.Tests.Caching
{
    [TestFixture]
    public class CacheTestsMe
    {
        private static IContainer _container;
        private static int mystep = 1;
        //private ICacheManager _cacheManager;

        [SetUp]
        public void Init()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new CacheModule());
            builder.RegisterType<DefaultCacheManager>().As<ICacheManager>();
            builder.RegisterType<DefaultCacheHolder>().As<ICacheHolder>().SingleInstance();
            builder.RegisterType<DefaultCacheContextAccessor>().As<ICacheContextAccessor>();
            builder.RegisterType<Signals>().As<ISignals>().SingleInstance();
            builder.RegisterType<ComponentOne>().As<IComponentOne>();
            builder.RegisterType<ComponentTwo>().As<IComponentTwo>();
            _container = builder.Build();
            //_cacheManager = _container.Resolve<ICacheManager>(new TypedParameter(typeof(Type), GetType()));


        }


        [Test]
        public void MyFirst()
        {
            //常规测试
            var com1 = _container.Resolve<IComponentOne>();

            var s = com1.Get().Name;
            System.Threading.Thread.Sleep(1000);
            var s1 = com1.Get().Name;
            System.Threading.Thread.Sleep(1000);
            var s2 = com1.Get().Name;

            var ss = s + s;

        }


        [Test]
        public void MyTwo()
        {
            //套叠测试
            var com2 = _container.Resolve<IComponentTwo>();
            //mystep 1
            //首次获取
            var s = com2.Get();
            //这是MyReturn测试！1
            Assert.That(s.Name, Is.EqualTo("这是MyReturn测试！1"));
            Assert.That(s.myReturn2.Name, Is.EqualTo("这是MyReturn2测试！1"));
            //加时，可检查缓存是否有效
            mystep++;//2

            //二次获取
            var s1 = com2.Get();
            Assert.That(s1.Name, Is.EqualTo("这是MyReturn测试！1"));
            Assert.That(s1.myReturn2.Name, Is.EqualTo("这是MyReturn2测试！1"));
            //使MyReturn2缓存失效
            com2.MyRefIn();
            //加时，可检查缓存是否有效
            mystep++;//3

            //三次获取
            var s2 = com2.Get();

            //内部失效，则以上层级失效
            Assert.That(s2.Name, Is.EqualTo("这是MyReturn测试！3"));
            Assert.That(s2.myReturn2.Name, Is.EqualTo("这是MyReturn2测试！3"));


            mystep++;//4

            //四次获取
            var s3 = com2.Get();
            Assert.That(s3.Name, Is.EqualTo("这是MyReturn测试！3"));
            Assert.That(s3.myReturn2.Name, Is.EqualTo("这是MyReturn2测试！3"));


            com2.MyRef();
            //加时，可检查缓存是否有效
            mystep++;//5

            //五次获取
            var s4 = com2.Get();

            //外部失效，不影响内部有效
            Assert.That(s4.Name, Is.EqualTo("这是MyReturn测试！5"));
            Assert.That(s4.myReturn2.Name, Is.EqualTo("这是MyReturn2测试！3"));


        }

        interface IComponentOne
        {
            MyReturn Get();
        }
        class ComponentOne : IComponentOne
        {
            private readonly ISignals _signals;
            private readonly ICacheManager _cacheManager;

            public ComponentOne(ICacheManager cacheManager, ISignals signals)
            {
                _cacheManager = cacheManager;
                _signals = signals;
                Name = "这是一个测试！" + mystep;
            }

            public MyReturn Get()
            {
                // _signals.Trigger("testItem1");
                return _cacheManager.Get("testItem", ctx =>
                    {
                        ctx.Monitor(_signals.When("testItem1"));
                        return new MyReturn();

                    });


            }


            public string Name { get; set; }

        }


        class MyReturn
        {
            public string Name { get; set; }
            public MyReturn()
            {
                Name = "这是MyReturn测试！" + mystep;

            }

            public MyReturn2 myReturn2 { get; set; }
        }


        class MyReturn2
        {
            public string Name { get; set; }
            public MyReturn2()
            {
                Name = "这是MyReturn2测试！" + mystep;

            }


        }

        interface IComponentTwo
        {
            MyReturn Get();

            void MyRefIn();


            void MyRef();


        }

        class ComponentTwo : IComponentTwo
        {
            private readonly ISignals _signals;
            private readonly ICacheManager _cacheManager;


            public ComponentTwo(ICacheManager cacheManager, ISignals signals)
            {
                _cacheManager = cacheManager;
                _signals = signals;

            }

            /// <summary>
            /// 使内部缓存失效MyReturn2
            /// </summary>
            public void MyRefIn()
            {
                _signals.Trigger("testItem3");
            }


            /// <summary>
            /// 使MyReturn缓存失效
            /// </summary>
            public void MyRef()
            {
                _signals.Trigger("testItem2");
            }
            public MyReturn Get()
            {
                // _signals.Trigger("testItem1");
                return _cacheManager.Get("testItemTwo", ctx =>
                    {
                        ctx.Monitor(_signals.When("testItem2"));
                        var result = new MyReturn();
                        result.myReturn2 = _cacheManager.Get("testItemTwo2", ctx2 =>
                        {
                            ctx2.Monitor(_signals.When("testItem3"));
                            return new MyReturn2();
                        });

                        return result;
                    });


            }
        }
    }
}