﻿using Autofac;
using Orchard.Caching;
using Orchard.Environment;
using Orchard.Services;
using System;
using System.Threading;

namespace ConsoleApp_OCache
{
    class Program
    {
        class TestA { }

        static void Main(string[] args)
        {

            ClockTest();

            Console.ReadLine();
        }

        static void ClockTest()
        {
            var container = OrchardStarter.CreateHostContainer(b => b.RegisterInstance(typeof(TestA)));
            var cm = container.Resolve<ICacheManager>();
            var clock = container.Resolve<IClock>();

            var i = 1;
            while (i < 15)
            {
                var tanew = cm.Get<string, TestA>("ta1", ccc =>
                {
                    ccc.Monitor(clock.When(TimeSpan.FromSeconds(4)));
                    return new TestA();
                });

                Console.WriteLine(tanew.GetHashCode());

                Thread.Sleep(TimeSpan.FromSeconds(1));

                i++;
            }

        }

    }
}
