﻿using System.Collections.Generic;

namespace Orchard.Caching
{
    /// <summary>
    /// 单例锚点提供器
    /// </summary>
    public interface ISignals : IVolatileProvider
    {
        void Trigger<T>(T signal);
        IVolatileToken When<T>(T signal);
    }

    /// <summary>
    /// 单例锚点提供器
    /// </summary>
    public class Signals : ISignals
    {
        /// <summary>
        /// 一个以 object 为 key 的键值对队列。
        /// Orchard中的单例对象队列（已知：缓存）
        /// </summary>
        readonly IDictionary<object, Token> _tokens = new Dictionary<object, Token>();

        /// <summary>
        /// 触发点。将 T signal 移除并置无效
        /// </summary>
        public void Trigger<T>(T signal)
        {
            lock (_tokens)
            {
                Token token;
                if (_tokens.TryGetValue(signal, out token))
                {
                    //?Why???
                    _tokens.Remove(signal);
                    token.Trigger();
                }
            }

        }

        public IVolatileToken When<T>(T signal)
        {
            lock (_tokens)
            {
                Token token;
                if (!_tokens.TryGetValue(signal, out token))
                {
                    token = new Token();
                    _tokens[signal] = token;
                }
                return token;
            }
        }

        class Token : IVolatileToken
        {
            public Token()
            {
                IsCurrent = true;
            }
            public bool IsCurrent { get; private set; }

            //
            public void Trigger() { IsCurrent = false; }
        }
    }
}
