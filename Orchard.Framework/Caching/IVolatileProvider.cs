﻿namespace Orchard.Caching
{
    /// <summary>
    /// “非稳定锚点”提供者。
    /// 主要用于支持多线程安全性问题。
    /// （volatitle修饰：“直接存取原始内存地址”。编译器生成的汇编指令指示，这种修饰的变量，都忽略CPU缓存中的数据，而变量的内存地址中直接读取）
    /// </summary>
    public interface IVolatileProvider : ISingletonDependency
    {
    }
}