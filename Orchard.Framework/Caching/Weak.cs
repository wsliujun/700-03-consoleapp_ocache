﻿using System;

namespace Orchard.Caching
{
    /// <summary>
    /// 泛型的弱类型对象。
    /// 使用弱类型的对象来使用某些不常用的，较为耗时耗内存的对象。（如文件监控，I/O操作对象等）。
    /// Weak对象 ，在引用对象的同时仍然允许垃圾回收来回收该对象。
    /// </summary>
    public class Weak<T>
    {
        private readonly WeakReference _target;

        /// <summary>
        /// 弱类型对象。
        /// 使用弱类型的对象来使用某些不常用的，较为耗时耗内存的对象。（如文件监控，I/O操作对象等）。
        /// Weak对象 ，在引用对象的同时仍然允许垃圾回收来回收该对象。
        /// </summary>
        public Weak(T target)
        {
            _target = new WeakReference(target);
        }


        /// <summary>
        /// 弱类型对象。
        /// 使用弱类型的对象来使用某些不常用的，较为耗时耗内存的对象。（如文件监控，I/O操作对象等）。
        /// Weak对象 ，在引用对象的同时仍然允许垃圾回收来回收该对象。
        /// </summary> 
        /// <param name="trackResurrection">对象复活后是否继续跟踪</param>
        public Weak(T target, bool trackResurrection)
        {
            _target = new WeakReference(target, trackResurrection);
        }

        /// <summary>
        /// 该弱类型对象的真正目标
        /// </summary>
        public T Target
        {
            get { return (T)_target.Target; }
            set { _target.Target = value; }
        }
    }
}