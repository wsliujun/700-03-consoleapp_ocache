﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Orchard.Caching
{
    public class Cache<TKey, TResult> : ICache<TKey, TResult>
    {
        /// <summary>
        /// 提供一个多线程唯一的 缓存 上下文 访问器（即当前缓存上下文访问器是线程安全的）。
        /// 唯一实现：DefaultCacheContextAccessor.Current。
        /// </summary>
        private readonly ICacheContextAccessor _cacheContextAccessor;
        /// <summary>
        /// 真正的缓存类型词典，被包装过.
        /// </summary>
        private readonly ConcurrentDictionary<TKey, CacheEntry> _entries;

        public Cache(ICacheContextAccessor cacheContextAccessor)
        {
            _cacheContextAccessor = cacheContextAccessor;
            _entries = new ConcurrentDictionary<TKey, CacheEntry>();
        }

        public TResult Get(TKey key, Func<AcquireContext<TKey>, TResult> acquire)
        {
            var entry = _entries.AddOrUpdate(key,
                // "Add" lambda
                k => AddEntry(k, acquire),
                // "Update" lambda
                (k, currentEntry) => UpdateEntry(currentEntry, k, acquire));

            return entry.Result;
        }

        private CacheEntry AddEntry(TKey k, Func<AcquireContext<TKey>, TResult> acquire)
        {
            var entry = CreateEntry(k, acquire);
            PropagateTokens(entry);
            return entry;
        }

        private CacheEntry UpdateEntry(CacheEntry currentEntry, TKey k, Func<AcquireContext<TKey>, TResult> acquire)
        {
            //判断一个CacheEntry是否失效，1、不为空，2、IsCurrent不为Fasle
            //两种失效类型 
            //   orchard缓存失效，有三种
            //1、签名失效  IVolatileToken.Trigger()  设置 IsCurrent = false;
            //2、时间失效  Clock.  _clock.UtcNow < _invalidateUtc;
            //3、??

            var entry = (currentEntry.Tokens.Any(t => t != null && !t.IsCurrent)) ? CreateEntry(k, acquire) : currentEntry;
            PropagateTokens(entry);
            return entry;
        }

        /// <summary>
        /// 传播 Tokens   ?????
        /// </summary>
        /// <param name="entry"></param>
        private void PropagateTokens(CacheEntry entry)
        {
            // Bubble up volatile tokens to parent context
            if (_cacheContextAccessor.Current != null)
            {
                foreach (var token in entry.Tokens)
                    _cacheContextAccessor.Current.Monitor(token);
            }
        }


        private CacheEntry CreateEntry(TKey k, Func<AcquireContext<TKey>, TResult> acquire)
        {
            var entry = new CacheEntry();
            var context = new AcquireContext<TKey>(k, entry.AddToken);

            IAcquireContext parentContext = null;
            try
            {
                // Push context
                parentContext = _cacheContextAccessor.Current;
                _cacheContextAccessor.Current = context;

                entry.Result = acquire(context);
            }
            finally
            {
                // Pop context
                _cacheContextAccessor.Current = parentContext;
            }
            entry.CompactTokens();
            return entry;
        }

        /// <summary>
        /// 缓存的实体。
        /// 每种缓存，其有Tokens集合，用于标示是否失效 ，IVolatileToken.IsCurrent。
        /// TResult用于存储真正的缓存内容本身
        /// </summary>
        private class CacheEntry
        {
            private IList<IVolatileToken> _tokens;
            public TResult Result { get; set; }

            /// <summary>
            /// 缓存是否有效的标记锚点
            /// </summary>
            public IEnumerable<IVolatileToken> Tokens
            {
                get
                {
                    return _tokens ?? Enumerable.Empty<IVolatileToken>();
                }
            }

            /// <summary>
            /// 附加 缓存是否有效的标记锚点。
            /// </summary>
            /// <param name="volatileToken"></param>
            public void AddToken(IVolatileToken volatileToken)
            {
                if (_tokens == null)
                {
                    _tokens = Enumerable.Empty<IVolatileToken>().ToList();// new List<IVolatileToken>();
                }

                _tokens.Add(volatileToken);
            }

            /// <summary>
            /// 清理重复的标记锚点
            /// </summary>
            public void CompactTokens()
            {
                if (_tokens != null)
                    _tokens = _tokens.Distinct().ToArray();
            }
        }
    }
}
