﻿using System;

namespace Orchard.Caching
{
    /// <summary>
    /// 基础ICache接口
    /// </summary> 
    public interface ICache<TKey, TResult>
    {
        /// <summary>
        /// 基础ICache接口 仅包含一个Get 方法
        /// </summary> 
        TResult Get(TKey key, Func<AcquireContext<TKey>, TResult> acquire);
    }
}
