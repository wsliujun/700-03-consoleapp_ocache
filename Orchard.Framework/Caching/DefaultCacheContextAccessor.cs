﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Orchard.Caching
{
    /// <summary>
    /// 提供一个多线程唯一的 缓存 上下文 访问器（即当前缓存上下文访问器是线程安全的）。
    /// 唯一实现：DefaultCacheContextAccessor.Current。
    /// </summary>
    public class DefaultCacheContextAccessor : ICacheContextAccessor
    {
        [ThreadStatic]
        private static IAcquireContext _threadInstance;

        /// <summary>
        /// 提供一个多线程唯一的 缓存 上下文 访问器（即当前缓存上下文访问器是线程安全的）。
        /// 唯一实现：DefaultCacheContextAccessor.Current。
        /// </summary>
        public static IAcquireContext ThreadInstance
        {
            get { return _threadInstance; }
            set { _threadInstance = value; }
        }

        /// <summary>
        /// 提供一个多线程唯一的 缓存 上下文 访问器（即当前缓存上下文访问器是线程安全的）。
        /// 唯一实现：DefaultCacheContextAccessor.Current。
        /// </summary>
        public IAcquireContext Current
        {
            get { return ThreadInstance; }
            set { ThreadInstance = value; }
        }
    }
}