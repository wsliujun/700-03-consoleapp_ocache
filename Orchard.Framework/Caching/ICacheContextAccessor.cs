﻿namespace Orchard.Caching
{
    /// <summary>
    /// 提供一个多线程唯一的 缓存 上下文 访问器（即当前缓存上下文访问器是线程安全的）。
    /// 唯一实现：DefaultCacheContextAccessor.Current。
    /// </summary>
    public interface ICacheContextAccessor
    {
        /// <summary>
        /// 提供一个多线程唯一的 缓存 上下文 访问器（即当前缓存上下文访问器是线程安全的）。
        /// 唯一实现：DefaultCacheContextAccessor.Current。
        /// </summary>
        IAcquireContext Current { get; set; }
    }
}