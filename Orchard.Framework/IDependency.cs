﻿

namespace Orchard
{
    /// <summary>
    /// 基础接口。用于web请求时，每个工作单元实例化的基础服务。
    /// Base interface for services that are instantiated per unit of work (i.e. web request).
    /// </summary>
    public interface IDependency
    {
    }

    /// <summary>
    /// 单例模式的基础接口。用于 shell 和 tenant(租户) 。
    /// Base interface for services that are instantiated per shell/tenant.
    /// </summary>
    public interface ISingletonDependency : IDependency
    {
    }

    /// <summary>
    /// 工作单元的基础接口。用于 shell 和 tenant(租户) 。
    /// Base interface for services that may *only* be instantiated in a unit of work.
    /// This interface is used to guarantee they are not accidentally referenced by a singleton dependency.
    /// </summary>
    public interface IUnitOfWorkDependency : IDependency
    {
    }

    /// <summary>
    /// //“瞬间对象”的基础接口
    /// Base interface for services that are instantiated per usage.
    /// </summary>
    public interface ITransientDependency : IDependency
    {
    }


    //public abstract class Component : IDependency
    //{
    //    protected Component()
    //    {
    //        Logger = NullLogger.Instance;
    //        T = NullLocalizer.Instance;
    //    }

    //    public ILogger Logger { get; set; }
    //    public Localizer T { get; set; }
    //}
}
