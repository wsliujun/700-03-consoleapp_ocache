﻿using Autofac;
using Orchard.Caching;
using Orchard.FileSystems.AppData;
using Orchard.FileSystems.VirtualPath;
using Orchard.FileSystems.WebSite;
using Orchard.Services;
using System;

namespace Orchard.Environment
{
    class CacheT { }
    /// <summary>
    /// Orchard 启动器
    /// </summary>
    public static class OrchardStarter
    {
        /// <summary>
        /// 静态方法，构造Host解析容器。
        /// </summary>
        /// <param name="registrations"></param>
        /// <returns></returns>
        public static IContainer CreateHostContainer(Action<ContainerBuilder> registrations)
        {
            var builder = new ContainerBuilder();
            //DefaultCacheManager构造函数有个参数是 Type，这里随便注册一个
            builder.RegisterInstance(typeof(CacheT));

            builder.RegisterModule(new CacheModule());


            builder.RegisterType<DefaultCacheHolder>().As<ICacheHolder>().SingleInstance();
            builder.RegisterType<DefaultCacheContextAccessor>().As<ICacheContextAccessor>().SingleInstance();


            #region RegisterVolatileProvider
            // 注册“非稳定锚点提供者”，.As(TService).As(IVolatileProvider)。
            // SingleInstance()为单个实例模式，换句话即全局单例。

            RegisterVolatileProvider<WebSiteFolder, IWebSiteFolder>(builder);
            RegisterVolatileProvider<AppDataFolder, IAppDataFolder>(builder);
            RegisterVolatileProvider<Clock, IClock>(builder);
            RegisterVolatileProvider<DefaultVirtualPathMonitor, IVirtualPathMonitor>(builder);
            // RegisterVolatileProvider<DefaultVirtualPathProvider, IVirtualPathProvider>(builder);

            #endregion

            registrations(builder);


            var container = builder.Build();


            return container;
        }


        /// <summary>
        /// 注册“非稳定锚点提供者”，.As(TService).As(IVolatileProvider)。
        /// .SingleInstance()为单个实例模式，换句话即全局单例。
        /// IVolatileProvider “非可靠对象”提供者接口，主要用于支持多线程安全性问题。
        /// </summary>
        private static void RegisterVolatileProvider<TRegister, TService>(ContainerBuilder builder) where TService : IVolatileProvider
        {
            builder.RegisterType<TRegister>()
                .As<TService>()
                .As<IVolatileProvider>()
                .SingleInstance();
        }

    }
}
