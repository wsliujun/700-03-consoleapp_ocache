﻿using System.Collections.Generic;
using System.IO;
using Orchard.Caching;

namespace Orchard.FileSystems.WebSite
{
    /// <summary>
    /// Web Site Folder 操作者。继承自 IVolatileProvider（非特定 “things” 提供者）。
    /// Abstraction over the virtual files/directories of a web site。
    /// </summary>
    public interface IWebSiteFolder : IVolatileProvider
    {
        IVolatileToken WhenPathChanges(string virtualPath);
    }
}