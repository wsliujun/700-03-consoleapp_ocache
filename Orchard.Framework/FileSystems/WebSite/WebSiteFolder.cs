﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Orchard.Caching;
using Orchard.FileSystems.VirtualPath;


namespace Orchard.FileSystems.WebSite
{
    public class WebSiteFolder : IWebSiteFolder
    {
        private readonly IVirtualPathMonitor _virtualPathMonitor;

        public WebSiteFolder(IVirtualPathMonitor virtualPathMonitor)
        {
            _virtualPathMonitor = virtualPathMonitor;
        }


        public IVolatileToken WhenPathChanges(string virtualPath)
        {
            return _virtualPathMonitor.WhenPathChanges(virtualPath);
        }

    }
}