using System;
using System.Collections.Generic;
using System.IO;
using Orchard.Caching;

namespace Orchard.FileSystems.AppData
{
    /// <summary>
    /// App_Data Folder 操作者。继承自 IVolatileProvider（非特定 “things” 提供者）。
    /// Abstraction of App_Data folder. All virtual paths passed in or returned are relative to "~/App_Data". 。
    /// Expected to work on physical filesystem, but decouples core system from web hosting apis
    /// </summary>
    public interface IAppDataFolder : IVolatileProvider
    {

        IVolatileToken WhenPathChanges(string path);

    }
}