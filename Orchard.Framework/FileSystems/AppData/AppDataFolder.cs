﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Orchard.Caching;
using Orchard.FileSystems.VirtualPath;

namespace Orchard.FileSystems.AppData
{
    public class AppDataFolder : IAppDataFolder
    {
        private readonly IAppDataFolderRoot _root;
        private readonly IVirtualPathMonitor _virtualPathMonitor;

        public AppDataFolder(IAppDataFolderRoot root, IVirtualPathMonitor virtualPathMonitor)
        {
            _root = root;
            _virtualPathMonitor = virtualPathMonitor;
        }

        public string RootFolder
        {
            get
            {
                return _root.RootFolder;
            }
        }

        public string AppDataPath
        {
            get
            {
                return _root.RootPath;
            }
        }

        /// <summary>
        /// Combine a set of virtual paths into a virtual path relative to "~/App_Data"
        /// </summary>
        public string Combine(params string[] paths)
        {
            return Path.Combine(paths).Replace(Path.DirectorySeparatorChar, '/');
        }

        public IVolatileToken WhenPathChanges(string path)
        {
            var virtualPath = GetVirtualPath(path);
            return _virtualPathMonitor.WhenPathChanges(virtualPath);
        }

        public string GetVirtualPath(string path)
        {
            return Combine(AppDataPath, path);
        }
    }
}