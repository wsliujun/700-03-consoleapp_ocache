﻿using Orchard.Caching;

namespace Orchard.FileSystems.VirtualPath
{
    /// <summary>
    /// 虚拟路径监听器，继承自 IVolatileProvider，一个实现者：DefaultVirtualPathMonitor。
    ///  
    /// IVolatileProvider “非稳定锚点”提供者，主要用于支持多线程安全性问题。
    /// （volatitle修饰：“直接存取原始内存地址”。编译器生成的汇编指令指示，这种修饰的变量，都忽略CPU缓存中的数据，而变量的内存地址中直接读取）。
    /// 
    /// Enable monitoring changes over virtual path
    /// </summary>
    public interface IVirtualPathMonitor : IVolatileProvider
    {
        /// <summary>
        /// 设置一个“虚拟路径变化”时的锚点。
        /// 
        /// 1、在全局单例的 DefaultVirtualPathMonitor._tokens 里绑定一个锚点；
        /// 2、向 System.Web.Host绑定监控信号（最终调用 HostingEnvironment.Cache.Add(....,callback)，依赖于.net框架的缓存监控回调方法，来实现监控）
        /// 
        /// </summary>
        /// <returns>一个 “非可靠”锚点。 忽略CPU缓存，直接读取内存，以支持多线程安全性问题。</returns>
        IVolatileToken WhenPathChanges(string virtualPath);
    }
}