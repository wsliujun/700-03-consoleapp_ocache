﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using Orchard.Caching;
//using Orchard.Logging;
using Orchard.Services;

namespace Orchard.FileSystems.VirtualPath
{
    /// <summary>
    /// 虚拟路径监听器，继承自 IVolatileProvider，一个实现者：DefaultVirtualPathMonitor，全局单例。
    ///  
    /// IVolatileProvider “非稳定锚点”提供者，主要用于支持多线程安全性问题。
    /// （volatitle修饰：“直接存取原始内存地址”。编译器生成的汇编指令指示，这种修饰的变量，都忽略CPU缓存中的数据，而变量的内存地址中直接读取）。
    /// 
    /// Enable monitoring changes over virtual path
    /// </summary>
    public class DefaultVirtualPathMonitor : IVirtualPathMonitor
    {
        private readonly Thunk _thunk;
        private readonly string _prefix = Guid.NewGuid().ToString("n");
        /// <summary>
        /// 一个有KEY的 弱类型锚点 词典
        /// </summary>
        private readonly IDictionary<string, Weak<Token>> _tokens = new Dictionary<string, Weak<Token>>();
        private readonly IClock _clock;

        public DefaultVirtualPathMonitor(IClock clock)
        {
            _clock = clock;
            _thunk = new Thunk(this);
            //Logger = NullLogger.Instance;
        }

        //public ILogger Logger { get; set; }

        /// <summary>
        /// 设置一个“虚拟路径变化”时的锚点。
        /// 
        /// 1、在全局单例的 DefaultVirtualPathMonitor._tokens 里绑定一个锚点；
        /// 2、向 System.Web.Host绑定监控信号（最终调用 HostingEnvironment.Cache.Add(....,callback)，
        /// 3、依赖于 Signal 的缓存监控,并在失效时运行指定的回调方法。
        /// </summary>
        /// <returns>一个 “非可靠”锚点。 忽略CPU缓存，直接读取内存，以支持多线程安全性问题。</returns>
        public IVolatileToken WhenPathChanges(string virtualPath)
        {
            //绑定锚点
            var token = BindToken(virtualPath);
            try
            {
                //向 System.Web.Host绑定监控信号（最终调用 HostingEnvironment.Cache.Add(....,callback)，
                //依赖于 Signal 的缓存监控,并在失效时运行指定的回调方法
                BindSignal(virtualPath);
            }
            catch (HttpException)
            {
                // This exception happens if trying to monitor a directory or file
                // inside a directory which doesn't exist
                //Logger.Information(e, "Error monitoring file changes on virtual path '{0}'", virtualPath);

                //TODO: Return a token monitoring first existing parent directory.
            }
            return token;
        }

        /// <summary>
        /// 绑定锚点，
        /// </summary>
        private Token BindToken(string virtualPath)
        {
            lock (_tokens)
            {
                Weak<Token> weak;
                if (!_tokens.TryGetValue(virtualPath, out weak))
                {
                    weak = new Weak<Token>(new Token(virtualPath));
                    _tokens[virtualPath] = weak;
                }

                var token = weak.Target;
                if (token == null)
                {
                    token = new Token(virtualPath);
                    weak.Target = token;
                }

                return token;
            }
        }

        /// <summary>
        /// 分离锚点(即删除)
        /// </summary>
        private Token DetachToken(string virtualPath)
        {
            lock (_tokens)
            {
                Weak<Token> weak;
                if (!_tokens.TryGetValue(virtualPath, out weak))
                {
                    return null;
                }
                var token = weak.Target;
                weak.Target = null;
                return token;
            }
        }

        /// <summary>
        /// 向System.Web.Host绑定监控信号（最终调用 HostingEnvironment.Cache.Add(....,callback)，依赖于.net框架的缓存监控回调方法，来实现监控）
        /// </summary>
        private void BindSignal(string virtualPath)
        {
            BindSignal(virtualPath, _thunk.Signal);

        }

        /// <summary>
        /// 向System.Web.Host绑定监控信号（最终调用 HostingEnvironment.Cache.Add(....,callback)，依赖于.net框架的缓存监控回调方法，来实现监控）
        /// </summary>
        private void BindSignal(string virtualPath, CacheItemRemovedCallback callback)
        {
            string key = _prefix + virtualPath;

            //PERF: Don't add in the cache if already present. Creating a "CacheDependency"
            //      object (below) is actually quite expensive.
            if (HostingEnvironment.Cache.Get(key) != null)
                return;

            //创建一个基于某虚拟路径的缓存依赖项，当该虚拟路径发生变化时，
            var cacheDependency = HostingEnvironment.VirtualPathProvider.GetCacheDependency
                (
                virtualPath,
                new[] { virtualPath },
                _clock.UtcNow);

            //Logger.Debug("Monitoring virtual path \"{0}\"", virtualPath);

            HostingEnvironment.Cache.Add(
                key,
                virtualPath,
                cacheDependency,            // 一个基于某虚拟路径的缓存依赖项
                Cache.NoAbsoluteExpiration, // 从不到期
                Cache.NoSlidingExpiration,  // 无时间间隔限制
                CacheItemPriority.NotRemovable, // 服务器自动清理内存时，该类型缓存不会被清理
                callback);                      //当所依赖的某虚拟路径变化时，回调方法
        }

        /// <summary>
        /// 信号通知。调用 DetachToken，从 _tokens 里分离锚点
        /// </summary>
        public void Signal(string key, object value, CacheItemRemovedReason reason)
        {
            var virtualPath = Convert.ToString(value);
            //Logger.Debug("Virtual path changed ({1}) '{0}'", virtualPath, reason.ToString());

            var token = DetachToken(virtualPath);
            if (token != null)
                token.IsCurrent = false;
        }

        /// <summary>
        /// 监控锚点，继承自 IVolatileToken “非可靠”锚点。
        /// 主要用于支持多线程安全性问题。
        /// </summary>
        public class Token : IVolatileToken
        {

            /// <summary>
            /// 监控锚点，继承自 IVolatileToken “非可靠”锚点。
            /// 主要用于支持多线程安全性问题。
            /// </summary>
            public Token(string virtualPath)
            {
                IsCurrent = true;
                VirtualPath = virtualPath;
            }
            public bool IsCurrent { get; set; }
            public string VirtualPath { get; private set; }

            public override string ToString()
            {
                return string.Format("IsCurrent: {0}, VirtualPath: \"{1}\"", IsCurrent, VirtualPath);
            }
        }

        /// <summary>
        ///  转换程序\转换器
        /// </summary>
        class Thunk
        {
            private readonly Weak<DefaultVirtualPathMonitor> _weak;

            /// <summary>
            ///  形实转换程序
            /// </summary>
            public Thunk(DefaultVirtualPathMonitor provider)
            {
                _weak = new Weak<DefaultVirtualPathMonitor>(provider);
            }

            /// <summary>
            /// 发出信号通知
            /// </summary>
            /// <param name="reason">CacheItemRemovedReason 指定从 System.Web.Caching.Cache 对象移除项的原因。【 1、Removed 移除的, 2、Expired 已过期, 3、Underused  要通过移除该项来释放内存, 4、DependencyChanged 关联的缓存依赖项已更改 】</param>
            public void Signal(string key, object value, CacheItemRemovedReason reason)
            {
                var provider = _weak.Target;
                if (provider != null)
                    provider.Signal(key, value, reason);
            }
        }
    }
}